module Main where
import Test.SmallCheck
import Examples.SmallCheckTest

main = do
  smallCheckResult idEmpProp
  smallCheckResult revProp
  smallCheckResult modelProp
  return 0

