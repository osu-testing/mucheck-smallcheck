{-# LANGUAGE StandaloneDeriving, DeriveDataTypeable, TypeSynonymInstances, FlexibleInstances, MultiParamTypeClasses #-}
-- | Module for using smallcheck properties
module Test.MuCheck.TestAdapter.SmallCheck where
import qualified Test.SmallCheck.Drivers as Sc
import Test.MuCheck.TestAdapter

import Data.Typeable

type SmallCheckSummary = Maybe Sc.PropertyFailure
deriving instance Typeable Sc.PropertyFailure

-- | Summarizable instance of `SmallCheckSummary`
instance Summarizable SmallCheckSummary where
  testSummary _mutant _test result = Summary $ _ioLog result
  isSuccess Nothing = True
  isSuccess _       = False


data SmallCheckRun = SmallCheckRun String

instance TRun SmallCheckRun SmallCheckSummary where
  genTest _m tstfn = "smallCheckResult " ++ tstfn
  getName (SmallCheckRun str) = str
  toRun s = SmallCheckRun s

  summarize_ _m = testSummary :: Mutant -> TestStr -> InterpreterOutput SmallCheckSummary -> Summary
  success_ _m = isSuccess :: SmallCheckSummary -> Bool
  failure_ _m = isFailure :: SmallCheckSummary -> Bool
  other_ _m = isOther :: SmallCheckSummary -> Bool

