hlint:
	(cd support; ~/.cabal/bin/hlint `find ../src -name \*.hs`)

clean-sandbox:
	- cabal sandbox hc-pkg unregister MuCheck-smallCheck

sandbox:
	mkdir -p ../mucheck-sandbox
	cabal sandbox init --sandbox ../mucheck-sandbox

build:
	cabal sandbox init --sandbox ../mucheck-sandbox
	cabal build

install:
	cabal sandbox init --sandbox ../mucheck-sandbox
	cabal install

run:
run:
	cabal sandbox init --sandbox ../mucheck-sandbox
	- rm *.tix
	cabal build sample-test
	./dist/build/sample-test/sample-test
	env MuDEBUG=1 ./dist/build/mucheck-smallcheck/mucheck-smallcheck -tix sample-test.tix Examples/SmallCheckTest.hs

prepare:
	cabal haddock
	cabal check
	cabal sdist

clean:
	- rm Examples/*_*
	- rm *.log


